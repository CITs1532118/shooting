<html>
    <head>
        <link rel="stylesheet" type="text/css" href="index.min.css">
        <?php
            $json = file_get_contents("score.json");
            $ranking = json_decode($json,true);
            $newScore = array("comment"=>$_POST['comment'], "score"=>$_POST['score']);
            array_push($ranking, $newScore);
            array_multisort(array_column($ranking, "score"), SORT_DESC, $ranking);
            $ranking = array_slice($ranking, 0, 10);
            $json = json_encode($ranking);
            file_put_contents("score.json", $json);
        ?>
    </head>
    <body>
        <h1 id="top">過去の栄光</h1>
        <a href="index.php" style="float: right;">帰るわ</a>
        <div id="ranking">
            <?php foreach($ranking as $rank) { ?>
                <div class="rank">
                    <p class="ranking-score"><?php echo($rank['score']) ?></p>
                    <p class="ranking-comment"><?php echo($rank['comment']) ?></p>
                </div>
                <hr>
            <?php } ?>
            repoは<a href="https://bitbucket.org/CITs1532118/shooting/src/master/">ココ!</a>
        </div>
    </body>
</html>

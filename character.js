
/**
 * Point - Used as position for character class.
 *
 * @property {number} x
 * @property {number} y
 */
function Point(){
    this.x = 0;
    this.y = 0;
}



function Character() {}

/**
 * Character - Parent class of player and enemy.
 *
 * @param  {associative array} params
 * @param   {number}  params.x
 * @param   {number}  params.y
 * @param   {number}  params.size
 * @param   {number}  params.hitPoint
 */
Character.prototype.init = function(params={}) {
    this.position = new Point();
    this.position.x =      params.x===undefined?0:params.x;
    this.position.y =      params.y===undefined?0:params.y;
    this.size =         params.size===undefined?5:params.size;
    this.hitPoint = params.hitPoint===undefined?1:params.hitPoint;
};

/**
 * Character.prototype.angleToTarget - Returns angle to target position as radian.
 *
 * @param  {type}   character target character
 * @return {radian}
 */
Character.prototype.angleToTarget = function(character){
    return Math.atan2(this.position.x-character.position.x, this.position.y-character.position.y);
};

Character.prototype.getPosition = function(){
    return this.position;
};

function Player() {
    this.super = Character.prototype;
}

Player.prototype = Object.create(Character.prototype);

function Enemy() {
    this.super = Character.prototype;
}

Enemy.prototype = Object.create(Character.prototype);

/**
 * Enemy
 *
 * @param  {associative array} params
 * @param   {number}  params.x
 * @param   {number}  params.y
 * @param   {number}  params.size
 * @param   {number}  params.hitPoint
 * @param   {string}  params.pattern
 * @param   {number}  params.patternFrame
 */
Enemy.prototype.init = function(params={}) {
    this.super.init(params);
    this.pattern =           params.pattern===undefined?"":params.pattern;
    this.patternFrame = params.patternFrame===undefined?0:params.patternFrame;
};

/**
 * Character.prototype.setPattern - Also sets patternFrame 0.
 *
 * @param  {string} pattern name of next pattern method.
 */
Enemy.prototype.setPattern = function(pattern){
    this.pattern = pattern;
    this.patternFrame = 0;
};

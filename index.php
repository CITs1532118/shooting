<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="index.min.css">
        <script src="projectile.js"></script>
        <script src="character.js"></script>
        <script src="main.js"></script>
    </head>
    <body>
        <canvas id="screen"></canvas>
        <p id="player"></p>
        <p id="score"></p>
        <form id=form action=score.php method=POST display=none>
            <input type=text name=comment value="no comment"><br/>
            <input type=hidden name=score value=0>
            <input type=submit value=スコア登録>
        </form>
    </body>
</html>

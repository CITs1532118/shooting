function Projectile () {}

/**
 * Projectile - Parent class of shot and bullet.
 * TODO extend this to shot class and bullet class
 *
 * @param {associative array} params={}
 * @param  {number}  params.x
 * @param  {number}  params.y
 * @param  {number}  params.size
 * @param  {number}  params.velocity
 * @param  {radian}  params.radian
 * @param  {boolean} params.isAlive
 * @param  {string}  params.color
 */
Projectile.prototype.init = function (params={}) {
    this.position = new Point();
    this.position.x =      params.x===undefined?0:params.x;
    this.position.y =      params.y===undefined?0:params.y;
    this.size =         params.size===undefined?1:params.size;
    this.velocity = params.velocity===undefined?0:params.velocity;
    this.radian =     params.radian===undefined?0:params.radian;
    this.isAlive =   params.isAlive===undefined?false:params.isAlive;
    this.color =       params.color===undefined?"purple":params.color;
};

/**
 * Projectile.prototype.set - Overwrites property of Projectile.
 *
 * @param  {associative array} params={}
 * @param  {Point}  params.position
 * @param  {number} params.size
 * @param  {number} params.velocity
 * @param  {radian} params.radian
 * @param  {string} params.color
 */
Projectile.prototype.set = function(params={}){
    this.position.x = params.position.x===undefined?this.position.x:params.position.x;
    this.position.y = params.position.y===undefined?this.position.y:params.position.y;
    this.size =             params.size===undefined?this.size:params.size;
    this.velocity =     params.velocity===undefined?this.velocity:params.velocity;
    this.radian =         params.radian===undefined?this.radian:params.radian;
    this.isAlive = true;
    this.color =           params.color===undefined?this.color:params.color;
};

/**
 * Projectile.prototype.move
 *  Move the position of instance by velocity and radian,
 *  kill the instance was it out of screen.
 */
Projectile.prototype.move = function(){
    this.position.x -= Math.sin(this.radian)*this.velocity;
    this.position.y -= Math.cos(this.radian)*this.velocity;

    if(this.position.x<0
        || this.position.y<0
        || this.position.x>256
        || this.position.y>512
    ){
        this.isAlive = false;
    }
};


/**
 * Projectile.prototype.hit - Check if the instance hits character. If so, decrease the character's hitPoint, return the result as boolean.
 *
 * @param  {Character} character character.js
 * @return {boolean}
 */
Projectile.prototype.hit = function(character){
    if(Math.sqrt(Math.pow(character.position.x-this.position.x,2)+Math.pow(character.position.y-this.position.y, 2))<this.size+character.size){
        character.hitPoint--;
        this.isAlive=false;
        return true;
    }
};

function Shot() {
    this.super = Projectile.prototype;
}
Shot.prototype = Object.create(Projectile.prototype);

/**
 * Shot.prototype.hit - Check if the instance hits character. If so, decrease the character's hitPoint, return the result as boolean.
 *
 * @param  {Character} character character.js
 * @return {boolean}
 */
Shot.prototype.hit = function(character){
    if(Math.sqrt(Math.pow(character.position.x-this.position.x,2)+Math.pow(character.position.y-this.position.y, 2))<this.size+character.size){
        character.hitPoint--;
        this.isAlive=false;
        return true;
    }
};

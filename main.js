//global
var screenCanvas;
var life;
var form;
var run = true;
var ctx; //canvas2d
var fire = false;
// character movement
var player;
var enemy;
var shot;
var purpleBullet;
var blueBullet;
var left;
var up;
var right;
var down;
var focused;
var charaVelocity;
var gameFrame;
var score;
var scoreBonus;
var multiplier;

let FPS = 1000 / 60;
let SCREEN_X = 256;
let SCREEN_Y = 512;
let MIN_X = 10;
let MAX_X = 246;
let MIN_Y = 10;
let MAX_Y = 502;

let PLAYER_SPEED = 5;
let PLAYER_COLOR              = 'rgba(255, 0, 0, 0.75)';
let ENEMY_COLOR               = 'rgba(255, 0, 255, 0.25)';
let ENEMY_HP_COLOR            = 'rgba(255, 0, 0, 1)';
let PLAYER_SHOT_COLOR         = 'rgba(0, 255, 0, 0.25)';
let ENEMY_BULLET_COLOR_PULPLE = 'rgba(255, 0, 255, 1)';
let ENEMY_BULLET_COLOR_BLUE   = 'rgba(0, 0, 255, 1)';
let ENEMY_HITPOINT_PER_PATTERN = 2000;
let PLAYER_SHOT_SIZE = 3;
let PLAYER_SHOT_VELOCITY = 20;
let PLAYER_SHOT_MAX_COUNT = 100;
let ENEMY_BULLET_MAX_COUNT = 1000;


/**
 * playerMove - Moves and draws player.
 */
function playerMove(){
    //TODO switch player speed when focused and moving diagonal
    charaVelocity = PLAYER_SPEED;
    if(focused)charaVelocity=charaVelocity/2;
    if((left||right)&&(up||down))charaVelocity=charaVelocity/Math.sqrt(2);

    // player movement
    if (left)player.position.x=player.position.x-charaVelocity;
    if (right)player.position.x=player.position.x+charaVelocity;
    if (up)player.position.y=player.position.y-charaVelocity;
    if (down)player.position.y=player.position.y+charaVelocity;

    // validate player position
    if (player.position.x<MIN_X)player.position.x=MIN_X;
    if (player.position.x>MAX_X)player.position.x=MAX_X;
    if (player.position.y<MIN_Y)player.position.y=MIN_Y;
    if (player.position.y>MAX_Y)player.position.y=MAX_Y;

    // draw player
    if(player.hitPoint<0)return false;
    ctx.fillStyle = PLAYER_COLOR;
    ctx.beginPath();
    ctx.arc(player.position.x, player.position.y, player.size, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.closePath();
}


/**
 * playerShot - Fires playerShot if shot key is down, also draws alive playerShots.
 */
function playerShot(){
    // fire shot
    if(fire){
        var shotParams = {
            position: player.position,
            size: PLAYER_SHOT_SIZE,
            velocity: PLAYER_SHOT_VELOCITY
        };
        for(var i=0;i<PLAYER_SHOT_MAX_COUNT;i++){
            if(!shot[i].isAlive){
                shot[i].set(shotParams);
                break;
            }
        }
    }

    //draw shots
    ctx.fillStyle = PLAYER_SHOT_COLOR;

    for(var i = 0; i < PLAYER_SHOT_MAX_COUNT; i++){
        if(shot[i].isAlive){
            shot[i].move();
            ctx.beginPath();
            ctx.arc(
                shot[i].position.x,
                shot[i].position.y,
                shot[i].size,
                0, Math.PI * 2, false
            );
            ctx.closePath();
            ctx.fill();
        }
    }
}


/**
 * shotHit - Checks if playerShot hits.
 */
function shotHit(){
    for(var i = 0; i < PLAYER_SHOT_MAX_COUNT; i++){
        if(shot[i].isAlive&&!!enemy&&enemy.hitPoint>0){
            if(shot[i].hit(enemy)){
                scoreBonus++;
                score += scoreBonus*multiplier;
            }
        }
    }
}


/**
 * drawEnemyHP - Draws enemy hp gauge.
 */
function drawEnemyHP(){
    if(!!enemy&&enemy.hitPoint>0){
        var enemyStack = Math.floor(enemy.hitPoint/ENEMY_HITPOINT_PER_PATTERN);
        ctx.beginPath();
        ctx.fillStyle = ENEMY_HP_COLOR;
        ctx.font="20px Georgia";
        ctx.textAlign="center";
        ctx.fillText(enemyStack, 10, 36);
        ctx.rect(20,20,enemy.hitPoint%ENEMY_HITPOINT_PER_PATTERN/ENEMY_HITPOINT_PER_PATTERN*200,20);
        ctx.closePath();
        ctx.fill();
    }
}

/**
 * enemyMove - Moves and draws enemy.
 */
function enemyMove(){
    if(!!enemy&&enemy.hitPoint>0){
        enemy.position.x=128+50*Math.sin(gameFrame/20);
        ctx.fillStyle = ENEMY_COLOR;
        ctx.beginPath();
        ctx.arc(enemy.position.x, enemy.position.y, enemy.size, 0, Math.PI * 2, false);
        ctx.fill();
        ctx.closePath();
    }
}


/**
 * fireBullet - Fires and draws enemy alive bullets.
 *
 * @param  {associative array} params
 * @param   {string}  params.color
 */
function fireBullet(params){
    params.color = params.color === undefined?"purple":params.color;
    params.position = enemy.position;
    if(!!enemy&&enemy.hitPoint>0){
        switch(params.color){
            case "purple":
                for(var i=0;i<ENEMY_BULLET_MAX_COUNT;i++){
                    if(!purpleBullet[i].isAlive){
                        purpleBullet[i].set(params);
                        break;
                    }
                }
                break;
            case "blue":
                for(var i=0;i<ENEMY_BULLET_MAX_COUNT;i++){
                    if(!blueBullet[i].isAlive){
                        blueBullet[i].set(params);
                        break;
                    }
                }
                break;
        }
    }
}

/**
 * fireNWay - Fires N way bullets to radian angle with span.
 *
 * @param {associative array} params
 * @param  {number} params.way
 * @param  {radian} params.radian
 * @param  {radian} params.span
 */
function fireNWay(params){
    params.way =       params.way ===undefined?3:params.way;
    params.radian = params.radian ===undefined?0:params.radian;
    params.span =     params.span ===undefined?0.05:params.span;

    params.radian -= (params.way-1)/2*params.span;
    for(var i=0;i<params.way;i++){
        fireBullet(params);
        params.radian += params.span;
    }
}

function patternA(){
    multiplier = 30;
    var angleToPlayer = enemy.angleToTarget(player);
    var params = {
        way: 17,
        size: 2,
        velocity: 5,
        radian: angleToPlayer,
        span: 0.1
    };
    if(enemy.patternFrame%30 === 0){
        fireNWay(params);
        params.way = 18;
        params.velocity = 3;
        params.radian = angleToPlayer;
        params.color="blue";
        fireNWay(params);
    } else if(enemy.patternFrame%30 === 15) {
        fireNWay(params);
    }
    if(enemy.patternFrame%80 === 0){
        for(var i=0;i<20;i++){
            params.way = 3;
            params.velocity = 2+i*0.4;
            params.radian = angleToPlayer;
            params.span = 0.1;
            fireNWay(params);
        }
    }
    enemy.patternFrame++;
}

function patternB(){
    multiplier = 20;
    var angleToPlayer = enemy.angleToTarget(player);
    var params = {
        way: 9,
        size: 3,
        velocity: 4,
        radian: 0,
        span: Math.PI/4.5
    };
    if(enemy.patternFrame%3===0) {
        params.radian = enemy.patternFrame*0.5;
        fireNWay(params);
        params.radian = enemy.patternFrame*1.5;
        params.color = "blue";
        fireNWay(params);
    }


    enemy.patternFrame++;
}

function patternC(){
    multiplier = 3;
    var angleToPlayer = enemy.angleToTarget(player);
    var params = {
        way: 21,
        size: 3,
        velocity: 4,
        radian: angleToPlayer,
        span: 0.1
    };
    switch(enemy.patternFrame%60) {
        case 0:
            params.velocity = 2;
            params.way = 20;
            params.color = "blue";
            fireNWay(params);
            break;
        case 10:
            params.velocity = 3;
            params.color = "blue";
            fireNWay(params);
            break;
        case 20:
            params.velocity = 5;
            params.way = 20;
            fireNWay(params);
            break;
        case 30:
            params.velocity = 6;
            fireNWay(params);
            break;
    }
    enemy.patternFrame++;
}

function patternD(){
    multiplier = 2;
    var angleToPlayer = enemy.angleToTarget(player);
    var params = {
        way: 14,
        size: 6,
        velocity:3,
        radian: angleToPlayer,
        span:0.05
    };
    switch(enemy.patternFrame%120) {
        case 0:
            fireNWay(params);
            break;
        case 30:
            fireNWay(params);
            break;
        case 60:
            fireNWay(params);
            break;
    }
    enemy.patternFrame++
}

function patternE(){
    multiplier = 1;
    var params = {
        way: 4,
        size: 6,
        velocity: 3,
        span: Math.PI/2
    };
    if(enemy.patternFrame%6===0){
        params.radian = enemy.patternFrame*0.5;
        fireNWay(params);
    }
    enemy.patternFrame++;
}


/**
 * enemyBullet - Fires and draws alive enemyBullets.
 */
function enemyBullet(){
    // fire bullet
    if(!!enemy&&enemy.hitPoint>0){
        eval(enemy.pattern+'()');
    }
    //draw bullets
    for(var i = 0; i < ENEMY_BULLET_MAX_COUNT; i++){
        if(purpleBullet[i].isAlive){
            purpleBullet[i].move();
            ctx.fillStyle = ENEMY_BULLET_COLOR_PULPLE;
            ctx.beginPath();
            ctx.arc(
                purpleBullet[i].position.x,
                purpleBullet[i].position.y,
                purpleBullet[i].size,
                0, Math.PI * 2, false
            );
            ctx.closePath();
            ctx.fill();
        }

        if(blueBullet[i].isAlive){
            blueBullet[i].move();
            ctx.fillStyle = ENEMY_BULLET_COLOR_BLUE;
            ctx.beginPath();
            ctx.arc(
                blueBullet[i].position.x,
                blueBullet[i].position.y,
                blueBullet[i].size,
                0, Math.PI * 2, false
            );
            ctx.closePath();
            ctx.fill();
        }
    }
}


/**
 * wipeBullets - Sets isAlive property of enemy bullets false.
 */
function wipeBullets(){
    for(i = 0; i < ENEMY_BULLET_MAX_COUNT; i++){
        purpleBullet[i].isAlive=false;
        blueBullet[i].isAlive=false
    }
}

function bulletHit(){
    for(i = 0; i < ENEMY_BULLET_MAX_COUNT; i++){
        if(purpleBullet[i].isAlive){
            if(purpleBullet[i].hit(player)){
                scoreBonus = 0;
                return wipeBullets();
            }
        }
        if(blueBullet[i].isAlive){
            if(blueBullet[i].hit(player)){
                scoreBonus = 0;
                return wipeBullets();
            }
        }
    }
}

function gameClear(){
    wipeBullets();
    ctx.font="20px Georgia";
    ctx.textAlign="center";
    ctx.fillStyle = "red";
    ctx.fillText("CONGRATULATIONS", 128, 200);
    run=false;
    form.id='form';
    form.score.value = score;
}

function gameOver(){
    ctx.font="20px Georgia";
    ctx.textAlign="center";
    ctx.fillStyle = "red";
    ctx.fillText("U SUCK", 128, 200);
    run=false;
    form.id='form';
    form.score.value = score;
}

/**
 * gameEvent - sets enemyPattern and etc.
 */
function gameEvent(){
    if (gameFrame === 60){
        var params = {
            x:128,
            y:10,
            size:50,
            hitPoint:5000
        }
        enemy = new Enemy();
        enemy.init(params);
        enemy.setPattern("patternE");
    }
    if(!!enemy&&enemy.hitPoint==4000){
        enemy.setPattern(player.hitPoint>13?"patternB":"patternD");
    } else if(!!enemy&&enemy.hitPoint==2000){
        enemy.setPattern(player.hitPoint>10&&enemy.pattern==="patternB"?"patternA":"patternC");
    }
    if(!!enemy){
        if(enemy.hitPoint<=0)gameClear();
    }
    if(player.hitPoint<0)gameOver();
    gameFrame++;
}


/**
 * window - main function
 */
window.onload = function(){
    screenCanvas = document.getElementById('screen');
    screenCanvas.width  = SCREEN_X;
    screenCanvas.height = SCREEN_Y;
    score = 0;
    scoreBonus = 0;
    multiplier = 1;
    ctx = screenCanvas.getContext('2d');

    window.addEventListener('keydown', keyDown, true);
    window.addEventListener('keyup', keyUp, true);

    life =      document.getElementById('player');
    scoreScreen = document.getElementById('score');

    form = document.getElementById('form');
    form.id='form-hidden';

    //initialize variables
    gameFrame = 0;
    var playerParams = {
        x: 128,
        y: 500,
        size: 4,
        hitPoint: 14
    }
    player = new Player();
    player.init(playerParams);
    player.position.x=128;
    player.position.y=500;
    shot = new Array(PLAYER_SHOT_MAX_COUNT);
    for (i=0;i<PLAYER_SHOT_MAX_COUNT;i++){
        shot[i] = new Shot();
        shot[i].init();
    }
    score = 0;
    scoreBonus = 0;

    purpleBullet = new Array(ENEMY_BULLET_MAX_COUNT);
    blueBullet = new Array(ENEMY_BULLET_MAX_COUNT);
    for (i=0;i<ENEMY_BULLET_MAX_COUNT;i++){
        purpleBullet[i] = new Projectile();
        purpleBullet[i].init();
        blueBullet[i] = new Projectile();
        blueBullet[i].init();
    }

    (function(){
        life.innerHTML = 'PLAYER:' + player.hitPoint;
        scoreScreen.innerHTML = 'SCORE: ' + score;


        ctx.clearRect(0, 0, screenCanvas.width, screenCanvas.height);

        gameEvent();
        playerMove();
        playerShot();
        shotHit();
        drawEnemyHP();
        enemyMove();
        enemyBullet();
        bulletHit();

        if(run){setTimeout(arguments.callee, FPS);}
    })();
};

function keyDown(event){
    // console.log("keyDown");
    // console.log(event.keyCode);
    var ck = event.keyCode;
    if(ck === 37){left = true;} //left
    if(ck === 38){up = true;} //up
    if(ck === 39){right = true;} //right
    if(ck === 40){down = true;} //down
    // if(ck === 27){run = false;} //esc
    if(ck === 90){fire = true;} //z
    if(ck === 16){focused = true;} //shift
}

function keyUp(event){
    // console.log("keyUp");
    // console.log(event.keyCode);
    var ck = event.keyCode;
    if(ck === 37){left = false;} //left
    if(ck === 38){up = false;} //up
    if(ck === 39){right = false;} //right
    if(ck === 40){down = false;} //down
    if(ck === 90){fire = false;} //z
    if(ck === 16){focused = false;} //shift
}
